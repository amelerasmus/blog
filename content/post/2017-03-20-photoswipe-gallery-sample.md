---
title: PERIODE 1
subtitle: GAMES
date: 2017-10-27
---


### DINSDAG 19 SEPTEMBER 2017
Vandaag hebben we een nieuwe uitleg over iteratie 2 gehad voor ons project. We moeten een strategie toepassen genaamd ‘kill your darling’ wat inhoud dat men heel het concept wat bedacht is in de prullenbak gooit en iets nieuws bedenkt. Dat komt mij wel goed uit want dit is mijn eerste schooldag. 
Mijn naam is Amel Erasmus en ik sta nu nog ingeschreven op de hogeschool in Utrecht voor een studie genaamd International Business and Languages wat echt niet bij me past. Daarom ben ik sinds deze week overgestapt naar CMD in Rotterdam. Ik ben erg enthousiast over deze studie omdat ik het heel erg leuk vind om diensten/producten te bedenken en ik wil graag het design stuk onder de knie krijgen dus bijvoorbeeld kunnen werken met fotoshop, InDesign en andere vergelijkbare applicaties. 
Mijn projectgroep heb ik vandaag leren kennen en op het moment zijn ze nog aan het struggelen met de kill your darling strategie. Ze vonden het erg jammer dat ze alles wat ze hadden bedacht moesten weggooien. 
Verder hebben we een beetje nagedacht over een nieuw concept maar daar zijn we niet echt ver mee gekomen. Ik had wel een paar ideeën, zo was dit een ideetje van me:

Bepaald thema foto’s maken
Bijvoorbeeld van een kind of een boom. Zo gaan ze met mensen praten. Uitdagend. 
Een boodschap neerzetten
Verschillende levels: uitbeelden gevoel
•	fotograferen mensen
•	Fotograferen dieren
•	Fotograferen voorwerpen
Dan op tijd door elkaarheen. 
Verschillende locaties .
Verschillende hoeken hoogtes
Drie teams spelen tegen elkaar, eentje controleert altijd de ander en zo wisselt het af.
Maar de groep was er niet erg enthousiast over dus ik ga na schooltijd even brainstormen voor een nieuw idee! 


### WOENSDAG 20 SEPTEMBER 2017
De enige les die ik vandaag had was de toepassende les van Design Theory. Het hoorcollege van afgelopen maandag had ik niet bijgewoond omdat ik gisteren pas officieel begonnen ben met deze studie dus daarom leek het me wel erg belangrijk bij deze toepassende les te zijn. 
De les ging over het toepassen van de theorie over de ‘double diamond model’
dus over het divergeren en convergeren van oplossingen en het bedenken van nieuwe concepten.
De opdracht was duidelijk, dat ging over hoe we met behulp van een voorwerp eerstejaars studenten meer verbondenheid met elkaar konden laten voelen. Wij hadden een nepplant als voorwerp gekozen. We moesten eerst heel breed en random gaan denken, zo kwam ik met oplossingen zoals: 
•	In de klas neerzetten en om de beurt water geven (wat niet kan aangezien het een nepplant is)
•	Alle eerstejaars verzamelen elke ochtend in een lokaal om de plant te aaien
•	Elke week mag iemand de plant mee naar huis nemen en houdt een dagboek bij met activiteiten die diegene met de plant uitvoert
•	De plant mag mee op klassen uitjes
•	In de plant zit een camera waardoor de nepplant foto’s kan maken
•	alle eerstejaars designen een nepplant voor elkaar
•	een plant die om de zoveel tijd muziek afspeelt waardoor iedereen random gaan dansen
Zoals je ziet waren deze ideeën nogal willekeurig. Daarna hebben we allemaal onze ideeën gedeeld en ze ingedeeld in 4 groepen: niet mogelijk, mogelijk in de toekomst, te simpel en kan werken.  Uiteindelijk hebben we twee ideeën uitgewerkt en daarvan een prototypen gemaakt. Een idee was een speaker in de plant zetten die je kan laten verbinden met je telefoon doormiddel van bluetooth waardoor de klas gezamenlijk naar iemand zijn muziek luistert én om de zoveel tijd random gaat dansen. 
Het tweede idee wat we hebben uitgewerkt was een nepplant met veel takken waar je briefjes met tips of algemene handige informatie aan kon ophangen. 

Ik vond het een interessante en vooral bruikbare les. 


### VRIJDAG 22 SEPTEMBER 2017

Vandaag hadden we weer Design Challenge, we hebben drie spellen getest en geanalyseerd. Tikkertje, verstoppertje en the floor is lava. Het was niet echt bruikbaar maar vooral erg leuk en gezellig om tussendoor als afleiding te doen! Daarna hebben we nagedacht over een concept. We gaan waarschijnlijk wat doen met levend cluedo maar we hebben er verder nog niet over nagedacht omdat we de doelgroep eerst nog verder moeten onderzoeken. Levend cluedo vind ik overigens wel een goed idee maar ik denk dat het nog best moeilijk valt uit te werken aangezien het interessant moet zijn voor eerstejaars fotografiestudenten dus ik weet nog niet helemaal hoe we dat gaan verwerken. Wat ik wel leuk vind is dat het natuurlijk een spannend spel is. 
Ik ben vandaag met Selen en Rutger onze doelgroep verder gaan onderzoeken. We zijn op speurtocht geweest bij Willem de Koning academie en we zijn wel een aantal mensen tegengekomen die ons konden helpen. We hebben drie interviewen gehouden met fotografie studenten waarvan twee eerstejaars, ik stelde de vragen en ging er ook wat dieper op in. Wat ik uit die interviews kon halen was dat ze alle drie graag hun realiteit doormiddel van fotograferen wilde laten zien aan de buitenwereld. Ik heb werk van ze gezien en ze hadden alle drie echt een hele andere eigen stijl. Wel sprak ik met de eerstejaars en die waren nog echt op zoek naar zichzelf en naar hun eigen stijl. Ik denk dat we daar misschien wel iets mee kunnen doen, we moeten eigenlijk een spel verzinnen waardoor ze zichzelf kunnen uitdagen meer opzoek te gaan naar hun eigenstijl. Ik vond persoonlijk dat ik goed ben in het interviewen van mensen, ik haalde er echt persoonlijke informatie uit wat bruikbaar is voor een onderzoek. In de verdere toekomst wil ik graag tijdens projecten meer onderzoeken. Want onderzoeken = weten. 

### DINSDAG 26 SEPTEMBER

Vandaag hebben we de hele dag spellen geanalyseerd en na gedacht over ons concept. We begonnen de dag met het spelen van monopoly, wie ben ik en pictionary. Dat was gezellig om te spelen, maar we konden er niet echt iets uit halen voor ons spel. We zitten te denken aan een moordspel zoals cluedo maar dan in het echt, wat handig is bij pictionary is dat je iets moet afbeelden dat willen we ook toepassen in ons spel maar dan met het fotograferen van bepaalde zaken, kunst. 
We liepen wel erg vast met het concept omdat we niet overal antwoorden op hadden, ik geloofde ook niet dat een levende versie van cluedo echt bij onze doelgroep paste omdat het toepassen van kunst en fotografie niet echt aan de orde kwam. Uiteindelijk hebben we aan het einde van de dag een mindweb gemaakt, hier heb ik veel aan meegedaan. Zo waren dingen die ik bedacht heb: wegwerpcamera, analoog etc. Ik denk dat het leuk is iets te doen met dat analoge. Daar was mijn groep het wel mee eens. In Willem de Kooning heb je namelijk een super vette DoKa dus misschien gaan we wat doen met het ontwikkelen van foto’s. Hoe lang dat duurt en hoe precies weet ik nog niet maar ga ik uitzoeken. Verder gingen we vandaag de interviews uitwerken, ik mocht er eentje doen. Van te voren had ik aangegeven dat ik de interviews graag wilde uitwerken. Ik vind dat niet zo erg maar voor de volgende keer..

### VRIJDAG 29 SEPTEMBER

Vandaag heb ik gewerkt aan de visuals, tegen ons team werd de vorige keer na onze presentatie gezegd dat de visuals beter mogen dus daar ben ik mee aan de slag gegaan. Ik heb gekozen voor Prezi omdat ik dat minder standaard vind dat powerpoint. De slides kun je helemaal je eigen maken met interessante effecten, het duurde wel even voordat ik erachter kwam hoe dat moest. Ik denk dat ik goed op weg ben. Voor de rest kon ik niet zoveel doen vandaag. 
Ik heb nog niet echt het idee dat ik precies weet waar ik mee bezig ben, dat komt deels ook omdat niet in natschool kan. Vind het vervelend dat ik zoveel achter mijn inschrijving aan moet zitten.. ik kan me ook niet inschrijven voor keuzevakken.


### MAANDAG 2 OKTOBER

Vandaag zou ik eigenlijk workshop hebben over de blog, hoe je verschillende thema’s kan aanpassen maar ik weet al niet hoe ik mijn blog online kan krijgen dus wilde ik graag naar die workshop gaan om dat voor elkaar te krijgen. Helaas kon ik deze workshop niet bijwonen want er was een grote storing op centraal station Utrecht, echt super vervelend. De 


eerst volgende intercity naar Rotterdam zou pas om twee uur weer rijden. Ik vind dit erg vervelend…
Voornamelijk omdat ik eigenlijk met mijn groepje zou afspreken voor en na de workshop. Nu hebben ze besloten dat Selen samen met de andere meiden de visuals voor de presentatie gaan afmaken wat ik wel jammer vind aangezien dat mijn taak was. Ik snap wel dat zij dat doen aangezien ik niet degene ben die moet presenteren, daarom vroeg ik of ik iets anders kon doen vanaf thuis en zei ik dat ik het gevoel had dat ik niet echt mee kon helpen. Helaas zeiden ze dat vandaag inderdaad niet kon wat me echt nutteloos liet voelen. 

### DINSDAG 3 OKTOBER

Vandaag op school is ons groepje de laatste dingetjes voor het inlevermoment aan het afronden, ik ben niets aan het doen wat ik jammer vind. Ik ben op –en nee naar de printer gelopen maar dat was mijn enige taak. Ik heb gevraagd of ik kan werken aan het ‘eindverslag’ zeg maar, maar dat was Sharons taak als dus daar werd nee tegen gezegd. Ik voelde me hier best wel rot om (niet per se om dat voorbeeld maar omdat ik niets kon doen). Hier heb ik niets over gezegd dat ik misschien wel beter kon doen. 
Toen ik weer in mijn kamertje in Utrecht was voelde ik me hier nog steeds slecht is omdat het niet is dat ik niets had willen doen maar eerder het gevoel kreeg dat ik de kans niet echt heb gekregen. Dat ligt dus deels zoals ik vrijdag al zei aan dat ik niet in het systeem kon en daardoor niet bij het benodigde lesmateriaal kon. Maar het kwam ook doordat ik me niet helemaal op mijn plek voelde in mijn groepje. Zij wisten heel goed wat ze moesten doen en ik had geen idee, heb wel veel gevraagd. Zoals vandaag van: ‘wat kan ik doen?’ en dan waren zij zo van: ‘ hmmm.. jaa weet ik eigenlijk niet. 
Dus dat was echt balen… MAAAAAAR, ’s avonds stuurde Sharon een appje met de vraag of ik het wel naar mijn zin had in het groepje omdat het wel een beetje leek als of ik het niet leuk had, waar ze deels gelijk in had. Ik vond het oprecht heel erg fijn dat ze me appte en daarom ben ik natuurlijk ook heel eerlijk geweest en ze begreep mijn situatie ook heel goed. We hebben het er een beetje overgehad en zijn tot de conclusie dat ik de volgende keer wel wat meer voor mezelf uit moet komen. Ook vinden we dat we voor de volgende iteratie een betere en strakke planning moeten maken zodat iedereen gewoon zijn eigen taken weet en doet. Ik vond het fijn haar gesproken te hebben.


### WOENSDAG 4 OKTOBER
Vandaag hadden we onze presentatie, vergeleken de andere groepjes werd ons concept als een van de betere beoordeeld door de docenten en een meisje dat CMD al heeft afgerond. Vragen waren er over het ontwikkelen van foto’s, of dat wel kon. Denk dat dat moeilijk 
gaat. Voor de rest hadden we niet super veel feedback. Vooral wat kleine dingetjes.

### DINSDAG 10 OKTOBER 
Balen… beide docenten zijn afwezig waardoor we geen uitleg kunnen krijgen over de nieuwe iteratie. Wel heb ik eindelijk een studentnummer wat echt erg fijn is. Het vervelende eraan is dat mijn moeder boos heeft moeten opbellen…Vind dat dingen op deze hogeschool beter geregeld kunnen worden. Ik kan bijvoorbeeld wel in natschool maar ben nog niet gekoppeld aan een klas dus ik kan er niets mee. 
Onze projectgroep is maar naar huis gegaan.

### DONDERDAG 12 OKTOBER
vandaag heb ik tentamen gehad, het was niet moeilijk maar waar ik achter ben gekomen is dat ik wel echt verkeerd geleerd heb, of in ieder geval niet alles. Ik heb namelijk het boek geleerd, maar er kwamen blijkbaar veel dingen in de hoorcolleges die niet in het boek staan, ik heb alleen het laatste hoorcollege bijgewoond aangezien ik toen pas begonnen ben met deze opleiding. Ik denk niet dat ik dit tentamen gehaald heb, maar voor de volgende het hertentamen moet ik de hoorcolleges terugkijken wat ik niet gedaan had.

### VRIJDAG 13 OKTOBER
 vandaag hebben ik en onze groep erg hard gewerkt voor de expositie over twee weken op woensdag. Ik heb in de ochtend een planning gemaakt wat ik fijn vond om te doen. Daarna heb ik meegeholpen aan wat we gaat doen aan onze plek bij de expo, we hebben bedacht een detective bord te maken, dus ik heb de kaart op A3 geprint. Ook ben ik begonnen met het schrijven van een pitch. Dat ga ik namelijk doen tijdens de expo. Het was echt een super nuttige dat, we waren als laatste op school.
Ik heb zo mijn zorgen over die competenties, ik vind ze op het moment niet helemaal duidelijk en ik ben bang dat ik ze niet haal aangezien mijn achterstand. Ik ga wel voor mezelf en de mensen die me beoordelen duidelijke STARTS schrijven. Ik weet dat ik taalkundig goed ben, dus dat moet lukken!


### DINSDAG 24 OKTOBER
vandaag nog een paar laatste dingetjes afgerond aan het detectivebord op school. Verder heb ik de pitch geschreven en die ben ik aan het voorbereiden. Dat moet me wel lukken want ik ben wel goed in het overtuigen van het publiek. 

### WOENSDAG 25 OKTOBER
vandaag de expo! Het was heel leuk, we zijn eerste geworden. Niet gekozen door de vakdocenten mar door de leerlingen. Ik was blij dat onze mede studenten ons idee zo leuk vinden. Nu lekker gaan werken aan de STARRT’s. 








