--- 
title: PERIODE 2
subtitle: PAARD
date: 2018-01-18
---

### DINSDAG 14 NOVEMBER
vandaag kick-off van het nieuwe project gehad. Dat was bij het poppodium het Paard in Den Haag het viel makkelijk te vinden en het was een leuke kick-off vond ik. De nieuwe opdracht lijkt me heel erg leuk, we moeten een product gaan bedenken voor het Paard namens Fabrique geloof ik. De teamindeling is nog niet bekend. 
Daarna werd er een film gedraaid, eigenlijk bleven er maar een paar mensen en ik was daar een van. Ik vond het nog best interessant, snap niet dat mensen niet bleven. Ik bedoel, het heeft wel te maken met je vakgebied dus snap niet waarom ze dat niet interessant genoeg vinden.
Ben erg benieuwd, er zijn een aantal veranderingen doorgevoerd door de school dus inschrijven voor workshops en dat soort dingen gaan online. Klinkt goed!

### VRIJDAG 17 NOVEMBER
vandaag is de groepsvorming gemaakt ik zit bij 4 andere meiden. Ik was zelf niet aanwezig maar ik ken de meiden wel allemaal een beetje en ik ben erg positief (tot nu toe) over mijn groepsgenoten.

### DINSDAG 21 NOVEMBER
vandaag is de dag dat we voor het eerst compleet waren en echt aan de slag gingen aan dit project. En we hebben hard gewerkt, ieder heeft een debrief getypt over de opdracht en die gaan we samenvoegen en we hebben een merkanalyse gemaakt (ik en Celine) maar ik kan nu al zien dat de merkanalyse veel te economisch was geworden. Het leek alsof ik weer international business and languages aan het doen was. Wel vond ik het leuk om samen te werken met Celine.

### VRIJDAG 24 NOVEMBER
gewerkt aan de user journey. Ik vind het oprecht wel leuk om te doen, zeker als team samen is het goed voor het project maar ook voor de bonding om echt iets samen te doen. Eliza was er niet, maar de rest wel. Ik denk dat we het nog best goed hebben gedaan. 
Even over de concepten die we bedacht hebben: ik kwam zelf met het idee heel veel paarden te verspreiden die je overal in den haag tegenkomt en waar een code opstaat die je doorverbind naar een site waar je korting krijgt. Mijn teamgenoten waren enthousiast. 
Andere ideeën van hun waren een combi-deal app dat je een avondje uit kan samenstellen (vond ik niet zo origineel, beetje voor de hand liggend) en een 360 graden scherm met live beelden van het paard. Daar was ik eigenlijk ook niet echt fan van want ik denk niet dat we dan echt mensen kunnen overhalen om naar het paard te komen.

Ik vond dat tijdens het bedenken van concepten mijn groepje niet echt geduld had. We moesten even snel 3 concepten hebben en dat vind ik jammer, want als je het gewoon wat meer de tijd geeft (of ja, niet elkaar onderdrukt) komt er iets veel originelers uit. Ik durfde dit niet zo goed aan te geven. 

### DINSDAG 28 NOVEMBER:
Vandaag gewerkt aan het prototype van mijn concept. Ik heb heel lang gewerkt aan het maken van de schermen waar je dan opkomt wanneer je de code scant (gewoon papier). Ik merk dat ik echt heel slecht ben in het visueel maken van dit soort dingen en daar wil ik echt goed in worden. 
We zaten een beetje in de knoop met ons groepje want onze drie concepten waren niet zodanig goed dat we er echt wat meekonden en volgende week moeten ze getest worden. 
Dat is natuurlijk heel erg vervelend maar dan zou ik denken, laten we wat nieuws bedenken maar sommige mensen stonden daar niet zo open voor. Gelukkig kwam bob ons helpen om wel onze gedachten wat meer open te krijgen. Maar toen was het al 5 uur en wilde iedereen naar huis dus het heeft uiteindelijk niet zoveel goeds gedaan wat ik oprecht jammer vind.
Ik wil best doorgaan met deze concepten maar iets nieuw bedenken dat wel antwoord geeft op alle ontwerpcriteria is denk ik beter en origineler.

### VRIJDAG 2 DECEMBER
vandaag was ik een beetje aan de late kant. Ik heb wel hard proberen te werken maar ik was moe. Ik heb gewerkt aan dagje Den Haag en aan het testplan. Ondanks dat ik te laat was ben ik wel tot 5 uur gebleven en heb ik geluisterd naar Chantal die uitleg gaf over hoe we feedback aan elkaar kunnen geven.

### DINSDAG 5 DECEMBER
vandaag de pitch voorbereid want we moeten ons concept morgen presenteren. Ben benieuwd wat de opdrachtgever ervan gaat vinden. Ik ben in principe wel goed in pitchen dus denk dat dat wel moet lukken.

### WOENSDAG 6 DECEMBER
om 10 uur hadden we dus die presentatie. Ik vond dat het goed ging, kreeg alleen de feedback dat ik net wat te zachtjes praatte maar ik was duidelijk en goed te verstaan.  Voor ons concept kregen we meer feedback over dat het meer leek op een marketingstunt waar ik het ook mee eens was maar dat het wel een tof concept was.
Ook zeiden ze nog dat we meer moesten focussen op hoe we de mensen uit onze doelgroep dan laten terugkomen naar het paard. Ik denk dat daar wel wat voor te verzinnen valt maar wat weet ik zo even niet. 

### DINSDAG 12 DECEMBER 
niet zo heel veel uitgevoerd helaas. Ben een beetje bezig met de ontwerpcriteria te verbeteren maar ik weet dus niet echt hoe ik het visueel moet maken. Daarom heb ik wat tutorials gekeken over InDesign en Illustrator. 

### DINSDAG 19 DECEMBER
had ik vergadering met de groep zonder Chantal wat ik raar vond. In elk geval een aantal mensen storen zich eraan dat ik soms maar ook Eliza en Celine en Dewi soms te laat komen. Dat kan ik me voorstellen. Daarom hebben we afspraken gemaakt dat het gewoon niet meer mocht. Verder heb ik niet zo heel veel te doen op school want ik heb alles al af en ik moet wachten op een prototype zodat ik een testplan kan schrijven voor ons eindconcept en het daarna kan gaan testen maar dit kan pas na de kerstvakantie. 

### DINSDAG 9 JANUARI
vandaag eerste dag school. Ik was lekker optijd en had er ook wel echt weer zin in. Een beetje gewerkt aan ontwerpcriteria maar ik krijg het in Indesign nog niet echt voor elkaar. Verder aan het wachten tot Zain en Celine eindelijk het prototype afgemaakt hebben.

### VRIJDAG 12 JANUARI
Zo'n beetje alles af wat ik af moest hebben maar helaas nog steeds geen protoype gezien en wil wel optijd gaan testen. Wel heb ik gewerkt aan het testplan dat ik nu ook al af heb omdat ik ongeveer weet hoe onze prototypes eruit gaan zien.

### MAANDAG 15 JANUARI
eindelijk het prototype ontvangen en zelf een prototype in elkaar geflanst met een houten paard. beide prototypes zijn niet echt klikbaar wat eigenlijk tegen de opdracht van de opdrachtgever ingaat..... maar er valt nu niet meer zoveel aan te doen want morgen zijn de validaties en komende week ergens zijn de deadlines.
Ik ben naar de koopgoot gelopen om mensen te vinden die het wilde testen maar iedereen was druk met shoppen of had geen zin. denk dat het ook aan het weer lag, het kwam met bakken uit de lucht.

### DINSDAG 16 JANUARI
vandaag validatie van het testplan, Pia doet dat heel erg goed en geeft erg nuttige feedback. Ik weet nu dat ik bij het centraal station moet gaan testen wat ik vandaag met Dewi heb gedaan. Echt super nuttige info!

### WOENSDAG 17 JANUARI
 Vandaag validatie van testresultaten die keihard werden afgekraakt door de Duitseleraar. Ik weet dat het niet mijn best verslag was maar als Pia het was geweest kreeg ik gewoon hele nutige feedback.
 'S avonds presentatie. De opdrachtgever is wezen kijken, het ging wel goed denk ik.


